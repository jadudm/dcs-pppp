
<h1>Announcements</h1>

<div class="posts">
  {% for post in paginator.posts %}
    <article class="post">
      <h2>
        <a href="{{ site.baseurl }}{{ post.url }}">{{ post.title }}</a>
      </h2>

      <div clsss="meta">
        <span class="date">
          {{ post.date | date: "%Y-%m-%d" }}
        </span>

        <ul class="tag">
          {% for tag in post.tags %}
          <li>
            <a href="{{ site.url }}{{ site.baseurl }}/tags#{{ tag }}">
              {{ tag }}
            </a>
          </li>
          {% endfor %}
        </ul>
      </div>

      <div class="entry">
        {{ post.excerpt }}
      </div>

      <a href="{{ site.baseurl }}{{ post.url }}" class="read-more">Read More</a>
    </article>
  {% endfor %}
</div>


<div class="container">
  
  <div class = "row">
    <div class = "col-sm">
      <strong>Assignment</strong>
    </div>
    <div class = "col-sm">
      <strong>Due</strong>
    </div>
    <div class = "col-sm-1">
      <strong>Upload</strong>
    </div>
  </div>
  
{% for ass in site.assignments %}
  
  <div class = "row">
    <div class = "col-sm">
      {{ass.title}}
    </div>
    <div class = "col-sm">
      <small><span id="due{{forloop.index0}}"></span></small>
    </div>
    <div class = "col-sm-1 text-center">
       <a href="" id="submit{{forloop.index0}}">
        <i class="fas fa-cloud-upload-alt"></i>
       </a>
    </div>
  </div>
  
{% endfor %}
</div>




<script>

function get(url) {
  // Return a new promise.
  return new Promise(function(resolve, reject) {
    // Do the usual XHR stuff
    var req = new XMLHttpRequest();
    req.open('GET', url);

    req.onload = function() {
      // This is called even on 404 etc
      // so check the status
      if (req.status == 200) {
        // Resolve the promise with the response text
        resolve(req.response);
      }
      else {
        // Otherwise reject with the status text
        // which will hopefully be a meaningful error
        reject(Error(req.statusText));
      }
    };

    // Handle network errors
    req.onerror = function() {
      reject(Error("Network Error"));
    };

    // Make the request
    req.send();
  });
}

function getJSON(url) {
  return new Promise(function (resolve, reject) { resolve(get(url).then(JSON.parse)) });
}


function getLists (boardID) {
  return getJSON("https://api.trello.com/1/boards/" + boardID + "/lists");
}

function getCards (listID) {
  return getJSON("https://api.trello.com/1/lists/" + listID + "/cards");
}

function getAllCards (boardID, render) {
  var timelineItems = [];
  return Promise.all(
    getLists(boardID)
  ).then (function (lists) {
    console.log(lists);
  }).then (function (cards) {
    console.log(cards);
  })
}

getLists("{{page.board}}")
  .then(function (list) {
     return getCards(list['id']); 
  }).then(function (card) {
    return console.log(card); 
  }).then(function (data) {
    
  });

/*
.then (function (card) {
          console.log(card)
          var obj = {};
          obj.id = ndx;
          ndx++;
          obj.content = card['title'];
          obj.start   = card['due'];
          obj.className = ls['name'];
          timelineItems.push(obj);
        });
        */
        
function renderTimeline (data) {
  console.log("Data is...");
  console.log(data);
  var dataset = new vis.DataSet(data);
  var container = document.getElementById('visualization');
  var options = {};
  // Create a Timeline
  var timeline = new vis.Timeline(container, dataset, options);
  console.log("Created timeline.");
  function onClick (props) {
    console.log(props);
  }
  timeline.on('click', onClick);
}

function loadTimeline (boardID) {
  // getAllCards(boardID, console.log);
  getAllCards(boardID, renderTimeline).then( function (result) {
    console.log(result);
  });
}

$(document).ready(loadTimeline("{{page.board}}"));
</script>

{% if site.posts.size == 0 %}
  <h2>No post found</h2>
{% endif %}

<ul class="archive">
  {% for post in site.posts %}
    {% unless post.next %}
      <h2>{{ post.date | date: '%Y' }}</h2>
    {% else %}
      {% capture year %}{{ post.date | date: '%Y' }}{% endcapture %}
      {% capture nyear %}{{ post.next.date | date: '%Y' }}{% endcapture %}
      {% if year != nyear %}
        <h2>{{ post.date | date: '%Y' }}</h2>
      {% endif %}
    {% endunless %}

    <li>
      {% if post.link %}
      <a href="{{ post.link }}">
      {% else %}
      <a href="{{ site.baseurl }}{{ post.url }}">
      {% endif %}
        {{ post.title }}
      </a>
      <time>{{ post.date | date: "%Y-%m-%d" }}</time>
    </li>
  {% endfor %}
</ul>

<hr>
<hr>

{% if site.posts.size == 0 %}
  <h2>No post found</h2>
{% endif %}

<div class="posts">
  {% for post in paginator.posts %}
    <article class="post">
      <h1>
        <a href="{{ site.baseurl }}{{ post.url }}">{{ post.title }}</a>
      </h1>

      <div clsss="meta">
        <span class="date">
          {{ post.date | date: "%Y-%m-%d" }}
        </span>

        <ul class="tag">
          {% for tag in post.tags %}
          <li>
            <a href="{{ site.url }}{{ site.baseurl }}/tags#{{ tag }}">
              {{ tag }}
            </a>
          </li>
          {% endfor %}
        </ul>
      </div>

      <div class="entry">
        {{ post.excerpt }}
      </div>

      <a href="{{ site.baseurl }}{{ post.url }}" class="read-more">Read More</a>
    </article>
  {% endfor %}
</div>

<div class="pagination">
  {% if paginator.previous_page %}
    <span class="prev">
      <a href="{{ site.baseurl }}{{ paginator.previous_page_path }}" class="prev">
        &#xE000; Previous
      </a>
    </span>
  {% endif %}
  {% if paginator.next_page %}
    <span class="next">
      <a href="{{ site.baseurl}}{{ paginator.next_page_path }}" class="next">
        Next &#xE001;
      </a>
    </span>
  {% endif %}
</div>



  
  get("https://api.trello.com/1/boards/" + boardID + "/lists").then(function(lists) {
    var jsonLists = JSON.parse(lists);
    //console.log("Success!", jsonLists);
    allCards = new Array();
    for (var lndx in jsonLists) {
      // console.log(lists[lndx]);
      get("https://api.trello.com/1/lists/" + jsonLists[lndx]['id'] + "/cards").then(function(cards) {
        jsonCards = JSON.parse(cards);
        for (cndx in jsonCards) {
          jsonCards[cndx]['listname'] = jsonLists[lndx]['name'];
          jsonCards[cndx]['listid'] = jsonLists[lndx]['id'];
          allCards.push(jsonCards[cndx]);
        }
      });
    }
    // console.log(allCards);
    return new Promise(function (resolve, reject) {
      resolve(allCards);
      });
  }, function(error) {
    console.error("Failed!", error);
    return [];
  })
}



function forEachCard (listName) {
  return function (cards) {
    
    for (ndx in cards) {
      console.log(listName + " - " + cards[ndx]);
      console.log(cards[ndx]);

      var obj = {};
      obj.id = timelineIndex
      timelineIndex++;
      obj.content = cards[ndx]['title'];
      obj.start   = cards[ndx]['due'];
      obj.className = listName;
      items.push(obj);
    }
  };
}

function forEachList (perCard) {
  return function (lists) {
    console.log(lists);
    for (var ndx in lists) {
      console.log(lists[ndx]);
       $.getJSON ("https://api.trello.com/1/lists/" + lists[ndx]['id'] + "/cards",
        perCard(lists[ndx]['name']));
    }
  };
}

function renderTimeline () {
  $.getJSON ("https://api.trello.com/1/boards/{{page.board}}/lists",
    forEachList(forEachCard));
    var dataset = new vis.DataSet(items);
    var container = document.getElementById('visualization');
    var options = {};
    // Create a Timeline
    var timeline = new vis.Timeline(container, dataset, options);
    console.log("Created timeline.");
    function onClick (props) {
      console.log(props);
    }
    
    timeline.on('click', onClick);

}
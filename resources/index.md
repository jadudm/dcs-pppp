---
title: Resources
layout: default
---

# Resources

Here's a collection of resources that either we've assigned for reading or think might generally be useful.

# Census Scans

If you are in DCS103: PPPP, [you should be able to access our scans of the LPS census data](https://drive.google.com/drive/folders/1YAj9dTY5iQ4yi6VonALKICTtYnUtHryq?usp=sharing).

We'll [enter our data here](https://goo.gl/forms/tLF8YkDO2gWlKXQf1).

# Notebooks

<div class="container">

{% assign sorted = site.data.notebooks | sort:"date" | reverse %}
{% for nb in sorted%}

<div class="row">
  <div class="col-md-3">
    <b>{{nb.name}}.ipynb</b>
  </div>
  <div class="col-md-5">
    {{nb.note}}
  </div>
  <div class="col-md-2">
    <a href="{{site.viewerurl}}/{{nb.path}}/{{nb.name}}.ipynb" target="_blank" >VIEW</a>
  </div>
  <div class="col-md-2">
    <a href="{{site.baseurl}}/{{nb.path}}/{{nb.name}}.ipynb" download>DOWNLOAD</a>
  </div>
</div>
<hr>
{% endfor %}
  
<br> &nbsp; <br>

<h1>Videos</h1>

<ul>
{% assign sorted = site.data.videos | sort:"date" | reverse %}
{% for video in sorted%}
<li> <a href="{{video.url}}" target="_blank" >{{video.name}}</a> <br>
     {% if video.note %}{{video.note}}{% endif %}
     </li>

{% endfor %}

  
<!-- 

# Class Readings

* [Making Refuge](https://drive.google.com/open?id=1N2a_yQnaNfnqMb72FB0ALkJOuXwuBEnR)
* [App Inventor](https://drive.google.com/open?id=1lOMbw8O8_mIejqBTJcw6rRj-c2StxYAQ)

-->
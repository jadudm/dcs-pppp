---
title: Libraries
layout: default
---

Python does not know how to do everything. Sometimes, you need to have some code installed to make things work.

In Jupyter Notebooks, this means dropping into the command line underneath the notebook and doing some installations.

## Open a Terminal

First, open a new terminal. This can be found under the "New" button.

![New Terminal](../images/terminal.png)

## pip install

Next, you're going to have to type a few commands. Typing a command is actually identical to clicking on an icon on a phone or a desktop/laptop. You're running a program. However, in this case, the interface is text. So, it *looks* intimidating, but the truth is, you're just *clicking an app the old fashioned way*.

Type each of these commands one at a time.

```csh

pip install plotly

pip install pandas

pip install geopy

```

## That's It!

I think that's everything. Sadly, once installed, I can't easily check what I've installed... so, I'm hoping that I haven't forgotten anything. 
test:
	jekyll s -P 8080

save:
	git add -A
	git commit -am "Autosave."
	git push
	
all:
	@echo "'make upload' to build and upload the site."
	
buildjadud:
	jekyll build --baseurl /teaching/dcs103f18
	
build:
	jekyll build --baseurl /courses/dcs103f18
	
# echo lynxrufus@lynxruf.us:/home/jadudm/jadud.com/teaching/dcs102w18

uploadjadud: buildjadud
	scp -r _site/* jadudm@jadud.com:~/jadud.com/teaching/dcs103f18/

upload: build
	rsync -avr -e "ssh" \
		--delete-after --delete-excluded \
		_site/ \
		lynxrufus@lynxruf.us:/home/lynxrufus/lynxruf.us/courses/dcs103f18/

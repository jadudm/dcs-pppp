---
week:   "2"
day:    "2"
title:  "Day 6: Properties of Entities"
type:   "In Class"
slug:   "Thinking about data..."
---

This homework is about programming, but it's a paper-based assignment. (Or, it could be a Google Doc. Perhaps an electronic document is better.) Point being, you are not writing *code*.

## Properties of Things

In this assignment, we're going to think about a number of *things* in the world, and try and give *names* to the *properties* that describe those *things*. We will also think about the *type* of information that we need to represent the properties. This begins to tie together some of the things from Chapter 1 and Chapter 2 in *Think Python* into our thinking about archives.

### Example: Representing a Bobcat

You are all Bobcats, because schools have this odd tradition of having mascots. Why? I don't know. But, as a result, you are all Bates Bobcats.

What information do we need to represent a Bates Bobcat? In a Google Doc, I would use a table to represent this information. [I have provided an example here.](https://docs.google.com/document/d/1Njy5Ptxr_LO-8IGxKbClzjrPcF3nKcaCVgLsQXnes-4/edit?usp=sharing) Note that I don't think it is even complete.

As you do this work, I would like you to think about things like:

* **Consistency**: How could you keep your data consistent? An example of this is where I reference the country code for where a student has their permanent residence.

* **Generality**: Is your approach to storing data *general*? For example, I have assumed everyone has a first and last name. However, this does *not* work well for people who effective have two family names, or (in the terminology of my form) two family names. In some countries, your *grandfather's name* is actually critical... and most US database systems have no way to represent this.

* **Specificity**: Is your breakdown specific? For example, I could have had a single field called "Adddress." However, this might have created a mess, so I tried breaking it apart into more specific, smaller pieces. 

## Things In The World

For each of these things, try and come up with a representation for them. We'll look at these in class, so be prepared to bring your GDoc up on a machine and talk with others about them. (You could print them, too, if you want.)

* You're a vet, and you want to keep track of pets brought into your practice. What information do you need?
* You're a chemist, and you want to keep track of chemicals in the stock room. 
* You're a bibliophile, and you have a lot of books. What information do you need to build your own database of books?
* You're scanning and digitizing school census data from a local school system. What information do you need to capture in order to represent the data about each log book? 
* You're still thinking about the scanning project... what information do you need to capture each entry in the log book?

## Submission

Save your work as a PDF (anonymously), and [upload it to Lyceum](https://lyceum.bates.edu/mod/assign/view.php?id=211500).



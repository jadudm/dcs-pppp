---
week:   "2"
day:    "0"
title:  "Day 5: Documenting Hate"
type:   "Reading"
slug:   "Hate is a poor diet."
---

For the weekend, you have several "readings." One is the [Frontline documentary *Documenting Hate*](https://www.youtube.com/watch?v=4ATHN9sAu28). It is approximately one hour long. This is a hard piece to watch.

You may find that [A.C. Thompson's written piece a good complement to the video](https://www.propublica.org/article/48-hours-in-charlottesville). This is not strictly required reading; it is written by the reporter who developed the Frontline piece. However, it is a second telling of the story of the video, and you may find having both the video and the text to be useful.

## Response

Go back and re-read the clergy's letter to MLK Jr. Imagine this letter arrived on August 11th, the day before the violence depicted in the Frontline piece. You, of course, did not see this letter, because you were taking part in the protests.

You have reflected on the clergy's letter from the perspective of the clergy, and the perspective of MLK Jr. Now, imagine this letter was written just days before the Charlottesville riots, which occured just two weeks before classes begin. Imagine that your coming to Bates was not this year, but last: this letter is circulating social media, the videos and news of the riots and deaths are front-and-center everywhere, and this is your arrival (or return) to college. You have been asked to write something about your summer---you were given wide lattitude. You choose to respond to the clergy.

Respond to their letter. Where do you agree, given what we have read and listened to so far about civial rights in the US, and what you know about the protests? Where do you disagree? Advance an argument: that is, you should have a point you want to make. Support that point by at least two (but not more than 4) clearly articulated perspectives. 

## Submission

Use Google Docs to write your response. Use the footnote feature to link to tweets, posts, or resources that you choose to leverage in your argument. 

When you are done, go to the "File" menu, "Download as...", and download the document as a PDF. Submit that PDF to Lyceum. Again, **do not include your name in the document itself**. I would like to read your responses without explicitly knowing who you are.

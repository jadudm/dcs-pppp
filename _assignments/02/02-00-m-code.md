---
week:   "2"
day:    "0"
title:  "Day 5: Fermi Problems"
type:   "Code"
slug:   "Get together with a friend, and have some fun."
---

For Monday, we'll continue practicing with Jupyter Notebooks. 

With a colleague in class (I will assign partners for the first round. Watch for an email.), first tackle the first three problems. *Everyone in the class will do the first three problems, so we can compare results.* 

Then, with your partner, tackle 10 more of these problems. Any ten! These have been harvested from around the web; a citation, on my part, is still needed.

A demonstration of one problem will be given in class, so you know how to format your work. 

## Submission 

When you're done, save the notebook, and submit the notebook file (the .ipynb file) to Lyceum.

I will randomly select a number of groups to share one of their calculations in class. (It will be random.) So, be prepared to share your logic.

## All Swim (Everyone Do These Three)

1. How many packets are needed to measure a single line of M&Ms to a distance of 100m?
1. How many people could you fit into our DCS 103 classroom? How many soccer balls?
1. If all the people in the world moved to Maine, how crowded would it be?

## Pick 10!

1. How old are you if you are a million seconds old? A million hours old? A million days old?
1. Could you fit $1,000,000 worth of $1 coins in your classroom? What about a billion dollars worth of $1 coins?
1. How much money is spent in the Den each day? In a week? Over the year?
1. If all the people in Australia joined hands and stretched themselves out in a straight line, how long would it reach?
1. How long would it take to count to a million?
1. How many cups of water are there in a bath tub? What about in an Olympic pool?
1. How many grains of rice are in a 10kg bag?
1. How many children are needed to have a mass the same as an elephant?
1. How many jelly beans fill a bucket?
1. How long would it take to drive to the moon (if you could!)?
1. What is the total mass in kilograms of all the students in your school?
1. Estimate the total number of hairs on your head.
1. Estimate the number of square inches of pizza consumed by all the students at the University of Maryland during one semester.
1. When it rains, water would accumulate on the roofs of flat-topped buildings if there were no drains. A heavy rain may deposit water to a depth of an inch or more. Given that water has a mass of about 1 gm/cm 3 , estimate the total force the roof of the physics lecture hall would have to support if we had an inch of rain and the roof drains were plugged.
1. One suggestion for putting satellites into orbit cheaply without using rockets is to build a tower 300 km high containing an elevator. One would put the payload in the elevator, lift it to the top, and just step out into orbit. Ignoring other problems (such as structural strain on the tower), estimate the weight of such a tower if its base were the size of Washington DC and it were made of steel. (Steel is about 5 times as dense as water, which has a density of 1 gm/cm^3.)
1. Estimate the total amount of time 19 year olds in the US spent during this past semester studying for exams in college. (Not counting finals.)
1. The deficit in the Federal Budget this past year was approximately $100 Billion ($10^11). Assuming this was divided equally to every man, woman, and child in the country, what is your share of the debt?
1.Related to the deficit: Supposing the deficit were paid in $1 bills and they were layed out on the ground without overlapping. Estimate what fraction of the District of Columbia could be covered.
1. Related to the deficit: Suppose you put these $1 bills in packages of 100 each and gave them away at the rate of 1 package every 10 seconds. If you start now, when will you be finished giving them away?
1. Related to the deficit: Are any of these calculations relevant for a discussion which is trying to understand whether the deficit is ridiculously large or appropriate in scale? Explain your reasoning.
1. Estimate the total number of sheets of 8.5 x 11 inch paper used by all the students at Bates College in one semester.
1. If the land area of the earth were divided up equally for each person on the planet, about how much would you get?
1. After the Gulf War, large areas of desert had to be cleared of mines using special bulldozers that simply sweep the sand in front of them like a snowplow, but whose blades are strong enough to withstand the explosion of a mine. Estimate how long it would take a single bulldozer to clear a patch of desert that is 10 km square.
1. A DVD for a computer stores information by flipping dye molecules embedded in the plastic of the disk. For a typical DVD (4.7 GB, or gigabytes), estimate the area of the disk that corresponds to a single bit of information. (Remember: the storage capacity of a disk is cited in bytes where 1 byte = 8 bits.)
1. How many notes (as in, notes in songs) are played on Spotify in a given year?
1. How many pencils would it take to draw a straight line along the entire Prime Meridian of the earth?
1. If all the string was removed from all of the tennis rackets in the US and layed out end-to-end, how many round trips from Detroit to Orlando could be made with the string?
1. How many drops of waters are there in all of the Great Lakes.
1. How many piano tuners are there in New York?
1. How many atoms are there in the jurisdiction of the continental US?
1. How far can a crow fly without stopping?
1. How many golf balls can fit in Pettengill Hall?
1. Estimate the number of cars and planes entering the state at any given time.
1. How much air (mass) is there in the room you are in?
1. How long does it take a light bulb to turn off?
1. How much milk is produced in the US each year?
1. How many flat tires are there in the US at any 1 time?

## Finally...

If you feel you still need more practice (which, learning to program takes practice), then you should do another 10 of these problems on your own. Or, with your partner. If you have questions, *you should be emailing me those questions*, or you should be [making an appointment]({{site.calendarbooking}}) and coming in to work with me. Practice, and asking questions, are two critical parts of success.

---
week:   "2"
day:    "4"
title:  "Day 7: Researching Stories"
type:   "Reading"
slug:   "Circling back from last week"
---

For Friday, please research a set of stories related to your topic. What this means:

1. *Create a document*. Start a Google Doc, and for each story you research, make notes of the critical elements of the story.
1. *Once Upon a Time*. Then, begin with a "once upon a time." All stories begin this way. For the subject of your story, imagine where they are from. Research that place. Find stories, images, video that help you get into the context of where they are from.
1. *Building Tension*. A traditional story line builds tension to a critical point. What are the events, people, places that frame two or three critical events in the life of the person you're writing about? 
1. *Climax*. What is the critical moment that your imaginary immigrant experiences? (I'm roughly working from a [classic dramatic arc here](https://en.wikipedia.org/wiki/Dramatic_structure#Freytag's_analysis).)
1. *Suggested Implications*. Instead of an entire "falling action" for the story arc, close with something that leaves the reader wondering: what are the implications for this person? What might, or might not, come to be? Guide our thoughts with enough suggestion that we feel some of the tension that the person in the story does.

**You're just collecting notes for each of these pieces**. Those notes might be bulleted lists of things you want to remember, links to resources, copied quotes from resources (keep track of where you pull from!), and so on. You're *not* writing the story yet. You're *thinking about writing the story, and organizing it.*

*I'm explicitly breaking out the writing process for you on this assignment, and making sure you have time to live with the material before trying to dive in and write.*

## Resources

<iframe width="100%" height="600" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQbBYKAoLaVKikmPb16NpewO9EDw_9M3n6KwLmUkgqXkvfeniuxJf6qhSkXjFYrPWUWxJp0cXWjgpxw/pubhtml?gid=0&amp;single=true&amp;widget=true&amp;headers=false"></iframe>

## Submission

PDF your document, make it anonymous (no names, please!), and submit it to Lyceum.

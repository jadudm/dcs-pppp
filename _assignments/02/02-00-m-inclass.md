---
week:   "2"
day:    "0"
title:  "Day 5: Q&A, Properties"
type:   "In Class"
slug:   "Intro to properties of data"
---

In class, we will begin with Q&A, and from there, there will be a short lecture about *properties*. This lecture will represent our first step towards thinking about how we might represent the tabular data in the Lewiston Public Schools census data. (And, more generally, how to think about representing *all* data.)

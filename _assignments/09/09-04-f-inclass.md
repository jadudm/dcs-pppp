---
week:   "9"
day:    "4"
title:  "Day 28: NO CLASS"
type:   "In Class"
slug:   "No class on Friday"
---

I will not be in town. There will be no class Friday.

Note that things are still due.
---
week:   "9"
day:    "4"
title:  "Day 28: Reparations Essay Due"
type:   "Writing"
slug:   "After all the revisions..."
---

In Lyceum.

I highly recommend you consider being "done" with this essay by the end of the weekend. Get together with a friend mid-week, and read each-other's essays.

If you're really, really serious about revision/quality writing, you will read each-other's essays out loud, one paragraph at a time, and make sure everything makes sense 1) within the paragraphs and 2) between the paragraphs. This will illuminate any final revisions that you feel you need to engage in.

## Request

Please include at the bottom of your essay an acknowledgement of who you worked with at various stages of this process. That might mean who you revised with in class, people you engaged in conversation as part of this work, and so on.

## Submission

Lyceum. Please submit these as anonymous documents, and as PDFs. Please do not, at this time, submit a Word doc. I really would like these as PDF documents.

PLEASE submit this to both Lyceum and to this [Google Form](https://goo.gl/forms/RvHprP6XQidb2ata2).
---
week:   "9"
day:    "0"
title:  "Day 26: Filtering Data"
type:   "Code"
slug:   "Filtering the Raw Data"
---

Because I will be traveling, I'm making the deadline on the data filtering notebook on Monday. Remember, the notebook and data are in Lyceum.

## Submission

Submit your notebook to Lyceum.

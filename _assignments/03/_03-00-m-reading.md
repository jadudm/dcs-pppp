---
week:   "3"
day:    "0"
title:  "Day 8: First Programs"
type:   "Reading"
slug:   "Studying Programming Chapters 4, 5"
---

[Chapter 4 of Studying Programming](https://lyceum.bates.edu/pluginfile.php/285262/mod_folder/content/0/studying-programming-004.pdf?forcedownload=1) is general overview; it should be a quick read. [Chapter 5](https://lyceum.bates.edu/pluginfile.php/285262/mod_folder/content/0/studying-programming-005.pdf?forcedownload=1) begins talking about the structure of code when writing programs.

I consider these good background chapters. I'm not sure they introduce a lot of new vocabulary or concepts. So, I would skim rather than read closely.

Note that our use of Notebooks as a space to explore programming is a little different than many "traditional" programming environments. That said, they're widely used for work in data analytics and repeatable analysis/scientific work. In short, computational notebooks did not exist when *Studying Programming* was written, or we may have featured them in the text!


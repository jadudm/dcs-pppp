---
week:   "3"
day:    "0"
title:  "Day 8: Functions (First Look)"
type:   "Code"
slug:   "What is your function?"
---

I have put together a notebook called {% include nblink file="Chapter_4" %} that parallels [chapter 4](http://greenteapress.com/thinkpython/html/thinkpython004.html) of *Think Python*. Put another way, *everything in the notebook comes directly from the chapter*. 

Read up to, and including, section 3.8. Do not go on to section 3.9. It's one step too far for this weekend.

You should:

1. **Read the chapter**. Take notes on things that you're uncertain about. 
2. **Explicitly note questions**. Write down questions that you know you have, as you're reading. "What does *blahblahblah* mean?" Or, "I don't get what section 4.x is saying... WTF?"
3. **Go through the notebook.** Work through the notebook, referencing the chapter as you go.
4. **Note questions**. Again, note questions. Feel free to ask questions, via email.

It is my intent that we will come in, engage in questions, and then do more practice in class. This is your first look at a new topic, so it is not my assumption that you will have mastered the material. It *is* my assumption that you will have questions, and need to continue exploring things further to understand the idea of functions, how to define them, what they're for, and so on.

*You are encouraged to work, actively, with a partner*. If you chose to do so,  note your collaboration in your notebook. And, make sure when you are done that you have not "gone along for a ride." You should have engaged as a full-peer collaborator.

## Submission

Submit your notebook to Lyceum before class. If you have attempted the notebook in its entirety, it will "meet expectations," and there is no space to "exceed expectations" on this work. It is a first look that is expected to possibly be confusing.
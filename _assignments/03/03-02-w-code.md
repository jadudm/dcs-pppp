---
week:   "3"
day:    "2"
title:  "Day 9: Practice"
type:   "Code"
slug:   "All the functions"
---

The following practice will be started in class, and will extend on to homework for Wednesday.

{% include nblink file="Ch4_Function_Practice" %}


We'll introduce the notion of 'if' statements in class, and your homework practice will include some 'if' probelems as well.

You want to engage with this by doing your best, and asking questions either via email or when we come into class. You can assume that we're going to continue practicing this material through the weekend.

## Submission

Please submit your practice notebook before class.
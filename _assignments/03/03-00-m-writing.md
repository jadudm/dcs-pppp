---
week:   "3"
day:    "0"
title:  "Day 8: The Immigrant Persona"
type:   "Writing"
slug:   "A portrait."
---

You've researched a type or kind of immigrant: the student, the parent, the single person, the child. These are not *all possible* types of human being that might be seeking to immigrate somewhere, but it's a reasonable set that we can relate to.

This weekend, write a 3-4 page portrait of the life of a person (based on your research) who has come to the USA. It should tell their origin story, the challenges of coming to the US, some critical or transformative events that may have occurred as part of that process (or perhaps after arriving), and close with how this person feels about the current climate in the US today. 

## Assessment

You've had a week of time to be reading and thinking about this work. As I look to assess, I'm going to consider writing that "goes through the motions" of telling the story I described above to **meet contract**. If you have really taken time to imagine what the lived experience of this person is, and to put yourself in their place... and, you've worked to convey that depth of consideration, then it will **exceed contract**.

It is likely that most of your writing will **meet contract**, because (for most of you) you will probably make your personal goal to "check all the boxes,"" as opposed to challenging yourself to go beyond what is described. "Going beyond" does not mean "writing a longer document," but it means that you have attempted to really put yourself in the place of another human being whose life is (probably) outside of your own lived experience.

## Submission

Your submission should be a PDF. You should not put your name at the top of the document; *it should be anonymous*. Upload it to Lyceum.

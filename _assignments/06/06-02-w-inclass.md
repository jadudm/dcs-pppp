---
week:   "6"
day:    "2"
title:  "Day 18: BREAK"
type:   "In Class"
slug:   "WYSIWYG."
---

No class. October recess.
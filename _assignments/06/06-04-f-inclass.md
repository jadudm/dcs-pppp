---
week:   "6"
day:    "4"
title:  "Day 19: BREAK"
type:   "In Class"
slug:   "AFK. TTFN."
---

No class. October recess.
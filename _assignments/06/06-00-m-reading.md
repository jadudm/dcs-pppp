---
week:   "6"
day:    "0"
title:  "Day 17: The Case for Reparations"
type:   "Reading"
slug:   "Listening recommended."
---

For Monday, please read [The Case for Reparations](https://www.theatlantic.com/magazine/archive/2014/06/the-case-for-reparations/361631/).

You are encouraged to listen to, rather than read, this piece. It is roughly 1h30m long. You should listen to this piece from multiple frames:

1. Last weekend, we read the chapter "Nationalities, Race, and Religion" from the book *Mapping Society* by Laura Vaughan. This book explored multiple maps through history that described (in more and less hateful ways) racial and religious distributions of peoples in urban settings, as well as how maps have been used in some contexts to enforce those separations. Vaughan spoke briefly about *redlining*, and Ta Nahisi Coates explores this idea further in his piece.
2. Earlier this term, we focused on MLK Jr., and the response of the religious establishment of the time to the civil rights protests of the time. Part of our work was to think about how the clergy's response would sound today, when removed from its historical context.
3. We listened to an interview with Ijeoma Oluo, author of *So you want to talk about race?*. We reflected on the post-interview questions that came in from listeners, and what elements of those questions made us feel particularly uncomfortable. This discomfort may have been because you were challenging some of your own understandings of race, or because you felt the listener's perspective was very different from your own.
4. We read stories of immigration, and authored our own, drawn from our collective research of several hundred stories. These stories explored questions of what it means to be "an American," what it is like coming to the USA today, and in writing them, many of us were asked (to a greater or lesser degree, depending on how deeply you engaged) to think about what it would be like to try and move to the US today. For some of us, we are visitors or immigrants, and these stories are less a challenge in terms of "what would it be like?," but rather a more personal and lived story.
5. We have begun to think about how we can automatically process data to tell a story. We have not yet learned enough to tell our own stories with data. However, we will soon be asking *who has the data*, *what stories do they choose to tell?*, and in asking these questions, *who has the power to define the past (and who does not)?*. This is a critical aspect of our work, and one you should keep in mind as you listen/read Ta Nahisi's piece.

**Remember**: my job is not to make you comfortable. My job is to force you to think critically—that means hard, and with evidence, and from many perspectives—about the world around you. Ta Nahisi Coates is presenting evidence and perspectives you may not be familiar or comfortable with.

## Response

**FOR MONDAY, OCTOBER 15**: Develop an outline for the prompt below. By "outline," I do not mean come in with five bullets. I mean develop a *rich* outline that provides detail down to the paragraph level for a whole essay, so that you have a clear sense for what points or arguments you would make, and what evidence or arguments you would offer to support each of those. This will allow you to come in and workshop the outline in a meaningful way. 

---

I've taken these questions from a packet by Daren Graves and Raygine DiAquoi titled *Critical Race Theory*. These questions frame a packet of readings including the Ta Nahisi Coates piece. (Wikipedia says that critical race theory is *is a theoretical framework in the social sciences that uses critical theory to examine society and culture as they relate to categorizations of race, law, and power*. This may sound familiar---we've spent the whole term exploring questions of people and place through lenses of power and the structures that build and bind our society.)

Engage with these four questions as you see fit. You could answer each of them directly, and you would *meet expectations* for the assignment. I would encourage you to instead extract a theme (or themes) from the Ta Nahisi Coates, think about that theme (or themes) in light of our prior readings and work, and develop a short essay exploring a thesis or idea of your own. In that case, these questions might serve as starting points, or seeds, for your own questioning.

1. Race is often described as a “social construction”. What does this mean to you? And, if we can agree that race is a social construct, to what end has race been socially constructed in the U.S. context?\
2. What, if anything, is the difference between racial prejudice/discrimination and racism?
3. What are the ways in which people can justify racist attitudes, dispositions, policies, and laws without engaging in the language or rhetoric of overt racism?

Although it would be a set of readings unto itself to explore fully, you might also consider this brief excerpt from *The Difference That Power Makes: Intersectionality and Participatory Democracy* by Patricia Hill Collins:

> Briefly stated, the heuristic has four main elements. Public policies that organize and regulate the social institution constitute the *structural domain of power*. Social hierarchy takes forms within social institutions such as banks, insurance companies, police departments, the real estate industry, schools, stores, restaurants, hospitals and governmental agencies. When people use the rules and regulations of everyday life and public policy to uphold social hierarchy or challenge it, their agency and actions shape the *disciplinary domain of power*. Increasingly dependent on tactics of surveillance, people watch one another and also self-censor by incorporating disciplinary practices into their own behavior. The *cultural domain of power* refers to social institutions and practices that produce the hegemonic ideas that justify social inequalities as well as counter-hegemonic ideas criticize unjust social relations. Through traditional and social media, journalism, and school curriculums, the cultural domain constructs representations, ideas and ideologies about social inequality. The *interpersonal domain of power* encompasses the myriad experiences that individuals have within intersecting oppressions.

---

Please bring your outline to class. It could be on paper, or electronic, as long as you can easily access it with a partner on Monday.


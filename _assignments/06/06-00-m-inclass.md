---
week:   "6"
day:    "0"
title:  "Day 17: Discussion, Planning"
type:   "In Class"
slug:   "What next?"
---

We will begin the day with a workshopping exercise on our outlines.

If there is time, we will consolidate our initial data transcription from Friday, so that I can further organize it for you over the recess.


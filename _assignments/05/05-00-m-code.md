---
week:   "5"
day:    "0"
title:  "Day 14: Boolean Practice"
type:   "Code"
slug:   "Practice makes TRUE"
---

In the [Resources]({{site.baseurl}}/resources) section, you'll find four new practice notebooks.

I recommend working 10-20 problems from both the "two expression" practice notebook and another 10-20 from the "four expression" practice notebook.

I think the 8- and 16-expression notebooks are just silly. But, feel free. (I think they're silly because I wrote code to generate them, and... I don't know if the plain-language version is actually readable. However, you can try.)

## How do I know how much to do?

If you do 10, and it's still slow work, do another 10. If you do 10, and it's easy, and you're getting every one of them right immediately, quit. 

The responsibility here is on you. **Here's what we're building towards**:

We are going to be working with data that we transcribe, and as we noted in class, we have interesting/complex questions to ask about that data. That means we will need to be able to identify what parts of our data we need, and then write functions to process that data, and identify which students/families/etc. are relevent to our inquiry. 

To do that, you need to be comfortable saying "I'll turn this question into a function," and write it. That's a *design process*, not a "wrote learning process." However, if you're trying to do interesting work---like asking research questions---and you're stuck on details---like the syntax for a function, or how to write an 'if' statement---then you'll never be able to do the interesting work.

So, we're doing some drill-and-practice. The design work---coming up with interesting research questions---is something you've already demonstrated in class that you can do. Now, getting to the point that you can write the code to support those inquiries... that's what I'm trying to help you build up to.

## Submission

I need to create a Lyceum spot. Upload your notebooks there.
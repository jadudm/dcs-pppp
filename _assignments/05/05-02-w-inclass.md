---
week:   "5"
day:    "2"
title:  "Day 15: Mapping All the Things"
type:   "In Class"
slug:   "Lists and Loops"
---

Lists and loops are why I write code.

Nothing is more satifsying than doing something 10,000,000 times, and only having to write a few lines of code.

*The learning you're doing now is what will save you thousands of hours in the future.*

Or, not.

But, being able to do things repetatively is why we have computers.

**THIS PAGE IS INCOMPLETE. TO BE FILLED IN WITH A NOTEBOOK TO PRACTICE PROGRAMMING WITH LISTS AND LOOPS**

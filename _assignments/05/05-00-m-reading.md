---
week:   "5"
day:    "0"
title:  "Day 14: Mapping and Society"
type:   "Reading"
slug:   "All the maps..."
---

In Lyceum, I've uploaded a chapter from Laura Vaughan's *Mapping Society: The Spatial Dimensions of Social Cartography*. The chapter is titled [Nationalities, Race, and Religion](https://lyceum.bates.edu/mod/resource/view.php?id=213684).

We'll be using this reading to understand some of our work going forward. Take notes. 

### Reading Strategy

Start by reading the headings of the chapter.

Then, read the introduction and conclusion of the chapter.

Then, read the first sentence of every paragraph.

Then, read the chapter.

## Response

Visit the [Sanborn Maps](http://sanborn.umi.com/) maps site. You'll need to select the state (Maine), city (Lewiston), and I recommend selecting the July 1914 maps. These are very close to when our census data was collected.

Look at one of your scans. Write down a half-dozen addresses for the students in your census. Find the map segment(s) that has your addresess(es). Print those segments, and mark where those students are on the map.

What have you discovered from this (very) small dive into the data. 

**Bring your maps to class to discuss**.
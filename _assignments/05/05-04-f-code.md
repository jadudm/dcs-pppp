---
week:   "5"
day:    "4"
title:  "Day 16: Continue the List Things"
type:   "Code"
slug:   ""
---

I highly recommend you get together with your partner Wednesday or Thursday evening, and do the following:

1. Continue working on the list notebook that I distributed today. **It is OK if you do not finish the entire notebook**. The very end of the notebook pokes some ideas that we'll need more time with. Cover the majority of it.
2. Revisit your notebook from Monday to Wednesday (your notes from the reading about lists). Compare where you see the same concepts in the notebooks, and where there are things that we still have not yet seen/explored.
3. Decide if you have questions. Or, things that you think you might have questions about. Either way, come prepared to start Friday with some Q&A.

**I've just described a productive, collaborative learning process**. If you choose to do this work yourself, you should still do the same things, but it will be harder. Talk through these ideas. Ask questions of each-other. Know that it is OK to say "I don't know," and then come in and ask your questions in class.

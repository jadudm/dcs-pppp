---
week:   "5"
day:    "2"
title:  "Day 15: Reading about Lists"
type:   "Reading"
slug:   "Eggs, a stick of butter, a gallon of milk..."
---

The simplest introduction to [what you can do with lists is on Google's developer webpages](https://developers.google.com/edu/python/lists).

1. Read through this once. 
2. Open a new notebook. Call it **Lists First Read**.
3. In a series of cells, explore every one of the list examples. (Actually, I'd ignore the bit about slices at the end. Ignore slices for now.)
4. For each example, try modifying it a bit. That is, see what breaks and what doesn't.
5. Bring questions to class.

As with other times that we're new to a subject, the assumption is that you will *not* master the material just from reading this webpage and creating a notebook. Instead, it **is** assumed that you will have *tried*, and you *will have questions*. We will then take a look at your questions in class.

In class, we'll work an example involving the creating lists of addresses, and adding those lists of addresses to a map. 

---
week:   "5"
day:    "0"
title:  "Day 14: Introduction to Mapping"
type:   "In Class"
slug:   "Now, where did I leave my keys?"
---

We'll begin by posing some questions from the reading, and preparing for conversation on Wednesday.

From there, we'll move on to our first exploration of mapping. 

## Download (and Upload) the Notebooks.

First, you're going to have to download two notebooks.

* PPPP100 : {% include nblink file="PPPP100" %}
* Intro to Mapping : {% include nblink file="IntroToMapping" %}

I would recommend you first create a folder in your Jupyter Notebooks area called "Mapping." Inside of that folder, upload these two files. (The PPPP100 notebook *must* be in the same folder as *any* notebook that you want to use for mapping.)

## Install the Libraries

You'll need to [install some libraries]({{site.baseurl}}/resources/libraries) before you can proceed further. Follow the instructions on the libraries page, and 

## Run the first cell

Run the first code cell in the **IntroToMapping** notebook. I want to know if the **import** statement works. If it doesn't... er, we need to fix something.

## Wait.

I know full-class walkthroughs are sometimes tedious. However, I'd like to make sure everyone has a working Mapping environment. At this point, after running the first cell, please wait.


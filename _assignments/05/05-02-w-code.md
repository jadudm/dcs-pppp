---
week:   "5"
day:    "2"
hour: "16"
minute: "30"
title:  "Day 15: Lewiston Codes!"
type:   "Plus One"
slug:   "+1 Opportunity."
---

On Wednesday, at Montello School, there is a district coding night. Volunteers to help families explore coding are needed and greatly appreciated.

You, as it happens, are *programming superwizardninjas*. 

[Here is a flyer about the event.](https://docs.google.com/document/d/1s_jTBQoJY5BPsG2MmLK46HaU6ydYsgDBL09bD6FWzwk/edit) If you're able to attend, let me know. I'll make sure the organizers know you're coming.

*This is a +1 opportunity. If you're on the edge between "Meets Contract" and "Exceeds Contract" at the end of the term, taking part in +1 opportunities helps push you over the edge. If you are unable to go, there will be others. The best way to do excellent work is to just do excellent work, not look for "bonus" opportunities.*

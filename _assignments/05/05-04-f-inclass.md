---
week:   "5"
day:    "4"
title:  "Day 16: Archiving Some Data"
type:   "In Class"
slug:   "Rinse, Repeat"
---

Depending on how things go with lists, we'll start with some archiving process on Friday.

[https://goo.gl/forms/5SIjLvITHD6C5WTi2](https://goo.gl/forms/5SIjLvITHD6C5WTi2)

As stated in class:

1. You can use the tab key to move between fields.
2. You should never have to take your hands off the keyboard.
3. One of you reads, one of you transcribes.
4. Hit return to activate the link to enter another address.

I think, with two of you working efficiently, you can transcribe the whole book in an hour. If it takes longer, it does.

(**NOTE TO MATT**: Do we have multiple years of scans? If we do, we need a way to indicate what year the book is from. Do we need to include the Ward information as we transcribe? For this minimal transcription, what do we *have* to have?)
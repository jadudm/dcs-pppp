---
week:   "8"
day:    "0"
title:  "Day 23: Just the Dinosaurs Submission"
type:   "Code"
slug:   "RAWRARAR"
---

Please upload your Dinosaur notebooks at this point. We started these in class, and you are now asked to submit the notebooks with your explorations and answer to the question "what theme is there in the data?"

Thank you.
---
week:   "8"
day:    "0"
title:  "Day 23: MOR TRANSCRIPTION"
type:   "Archive"
slug:   "Do some more transcription."
---

If you and your partner could please find roughly 1-2 hours to complete your transcription, that would be lovely. 

It should only take that long. If you work together, your spreadsheet should come together very quickly.

When you are done, download your spreadsheet as a CSV (comma-separated values) file. Submit that file to Lyceum.

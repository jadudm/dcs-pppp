---
week:   "8"
day:    "4"
title:  "Day 25: Digital Music Symposium"
type:   "In Class"
slug:   "CLASS IN OLIN 105"
---

![](https://www.bates.edu/music/files/2018/05/Symposium-Web-900x518.jpg)

[https://www.bates.edu/music/digitalsymposium/](https://www.bates.edu/music/digitalsymposium/)

Instead of class, we will be attending the first three performances in the digital music symposium. 
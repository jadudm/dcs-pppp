---
week:   "8"
day:    "2"
title:  "Day 24: Cleaning Data"
type:   "In Class"
slug:   "It's worse than it appears..."
---

I've [consolidated our raw data](https://docs.google.com/spreadsheets/d/1s4wqvoxR1EHkj52eYR5OAA6FNWSpZJdtnOWkfUsaGwE/edit?usp=sharing). Today, in class, I will do a live programming demo where we start to see what is involved in cleaning data for use.

We will also set up our homework, which is to write exam questions. We'll have a short activity in class to set up that work.
---
week:   "8"
day:    "4"
title:  "Day 25: Exam Questions"
type:   "Writing"
slug:   "Just one."
---

You should have left class with a topic for an exam question. Your challenge:

1. **It should be interesting**. We're all tired of boring exam questions.
2. **It should be relevant.** [Revisit the syllabus](https://lynxruf.us/courses/dcs103f18/admin/). Is the question related to the overarching goals of the course?
3. **It should be fair**. Is it in keeping with the learning you did this term?

This is not the space to say "is this too hard?," but instead you should be asking "is this assessing the learning we've engaged in this term?" It may be that you realize that you have not engaged as fully as you could have, and therefore the questions you're generating are *really hard*. However, if you've been working consistently, then you should be generating questions that are in the space of *potentially challenging, but consistent and fair in light of what we've done*.

I look forward to seeing what you come up with.

## Submission 

Submit your work as a Google Doc. This will make it easier for me to work with later. Please do not use Word, or Open Office, and please do not submit it as a PDF. This is not anonymous.

If you want to incorporate code (it seems reasonable), then please download the Notebook you develop as an ipynb file, and upload that as well.

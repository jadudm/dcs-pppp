---
week:   "8"
day:    "0"
title:  "Day 23: Reparations, Revised"
type:   "Writing"
slug:   "One last revision pass"
---

For your final revision on this essay, please print it out one more time.

You should have already gone through the document after our paragraph-level editing, and be prepared now for sentence-level editing. 

This will complete our revision process on this document. This is, I believe, what is required of any good writing process:

1. **A discussion of structure and themes**. We did this with our outline and initial conversation.
2. **An examination of draft at the paragraph level**. Does each paragraph advance the theme of the paper, and does each paragraph make sense?
3. **The sentence level**. Does the document hold together on a sentence-by-sentence basis?

You could then get down one level further, but for now, this will do.

Print and bring your essay to class for our sentence-level editing process.
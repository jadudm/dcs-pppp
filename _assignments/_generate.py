folders = [ "inclass", "archive", "reading", "writing", "code" ]
typeof =  [ "In Class", "Archive", "Reading", "Writing", "Code" ]
shortcuts = ["inclass", "archive", "reading", "writing", "code"]

MAXWEEK = 14
DAYNUMS = [0, 2, 4]

import os
def touch(path):
    with open(path, 'a'):
        os.utime(path, None)

def pad(n):
    if n < 10:
        return "0" + str(n)
    else:
        return str(n)

def dow(d):
    days = ["m", "t", "w", "r", "f", "s", "su"]
    return days[d]

for f, sc, to in zip(folders, shortcuts, typeof):
    daynum = 0
    for w in range(0, MAXWEEK):
        for d in range(0, 6):
            inp  = open("_template.md")
            thefile = pad(w) + "/" + "_" + pad(w) + "-" + pad(d) + "-" + dow(d) + "-" + f + ".md"
            if not os.path.isdir(pad(w)):
                os.mkdir(pad(w))
        
            if d in DAYNUMS:                
                outp = open(thefile, "w")
                for line in inp:
                    print (thefile)
                    line = line.replace("DAYNUM", "Day " + str(daynum - 1 ))
                    line = line.replace("DAY", str(d))
                    line = line.replace("WEEK", str(w))
                    line = line.replace("CATEGORY", to)
                    outp.write(line)
    
                daynum = daynum + 1

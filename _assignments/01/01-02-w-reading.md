---
week:   "1"
day:    "2"
title:  "Day 3: Chapter 1, Think Python"
type:   "Reading"
slug:   "We'll work with this in class."
---

Please read [Chapter 1](http://greenteapress.com/thinkpython2/html/thinkpython2002.html) and [Chapter 2](http://greenteapress.com/thinkpython2/html/thinkpython2003.html) from *Think Python*.

You can either view the [text on the WWW](http://greenteapress.com/thinkpython2/html/index.html), or you can also [download a PDF](https://greenteapress.com/wp/think-python-2e/) of the entire book. This book is made available under a free license, as a gift to you by the author Allen Downey, a Professor at Olin College in Needham, Massachusetts.

**Update 2018-09-10 22:23**: We wil be working with this material in class on Friday. This is a *first reading*. It is a chance for you to read, ask questions in class, and revisit before we dive into the work of programming. There is no specific response or action required, but you are *always* encouraged 1) take notes and, more specifically, 2) make notes of questions that you have. 
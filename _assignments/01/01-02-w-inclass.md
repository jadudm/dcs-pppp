---
week:   "1"
day:    "2"
title:  "Day 3: DooO and You"
type:   "In Class"
slug:   "Catapult, DooO, and towards portfolios"
---

In class, we'll talk about domains of one's own, the ideology behind them, and how to scaffold your way towards portfolios.


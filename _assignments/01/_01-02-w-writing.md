---
week:   "1"
day:    "2"
title:  "Day 3"
type:   "Writing"
slug:   ""
---


## Assignment: Research

The oppression and incarceration of people who are not "of the majority" is not new in America. Part of our work this semester will be to see the long history Americans have of oppressing and imprisioning people based on *how they look* or *where their parents were from*. 

1. Your first challenge is to find a video and written piece that praises or otherwise supports the "illegal" children of immmigrants who were incarcerated during the summer of 2018. 
2. Your second is to find a video and written piece that challenges or otherwise refutes that point of view. That is, one that condemns the children's incarceration. 
3. Your third challenge is to find a video and written piece that praises the incarceration/oppression of immigrants or an underrepresented group at some point in US history.
4. Your final challenge is to find a video and written piece that challenges that condemnation.

These videos and written pieces need not all be the same/from the same source. Attempt to find *reasoned* pieces, as much as you can. For example, a random, ranty blog post is unlikely to qualify; opinion pieces in major publications (newspaper/magazine), feature articles, or similar are far more likely to serve you well.


## Assignment: Writing

Your job is to*synthesize* across these pieces. Identify three or four themes in the "anti" pieces, and explain what you see as the commonalities/roots of these commonalities. Similarly, for the pieces that are "for," what themes emerge?

Finally, pause for a moment, and *reflect*. Are the stories of the people you researched part of your own family's past? If so, how? If not, might they be part of the history of friends? Classmates? What, if anything, did you *feel* while doing this research and writing?

# Submission

First, submit URLs for your resources, and a short description of each.

Second, submit your written synthesis and reflection.

In class, we will start in small groups and discuss our research, and consolidate themes from across the class.
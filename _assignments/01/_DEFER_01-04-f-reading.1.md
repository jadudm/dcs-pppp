---
week:   "1"
day:    "4"
title:  "Day 4: Pathways"
type:   "Reading"
slug:   "How do you get here, what is it like?"
---

You will be assigned, in class, one of the following four case studies. For your case study, please research the following questions, write a reflection in response to the prompt below, and come prepared to both teach your findings to others.

# Case Study 1: The Student

# Case Study 2: The Parent

# Case Study 3: The Child

# Case Study 4: The Single Person


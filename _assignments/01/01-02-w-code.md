---
week:   "1"
day:    "2"
title:  "Day 3: Catapult"
type:   "Code"
slug:   "Launching you for the term"
---

*Catapult* is a new service at Bates that we, in DCS 103 and DCS 104, will be piloting this term. Nifty, eh?

For homework, log into [Catapult](https://catapult.bates.edu/), and create a Wordpress blog. We're going to delete this blog, so this is *entirely for practice*.

1. Change the theme. How does that effect things?
2. Write a post. Post a meme, tell a joke... 
3. Write a *page*. This is different than a post. Can you figure out where it went?
4. Experiment with the *modules* that are part of Moodle. Perhaps that helps with #3...

## Submission

When you're done, submit the URL to your blog to the submission form. 

We'll be spending class on doing work with Catapult. There's a number of things to consider, as we work with this new technology. 

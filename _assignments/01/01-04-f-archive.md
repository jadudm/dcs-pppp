---
week:   "1"
day:    "4"
title:  "Day 4: Introduction to Archives"
type:   "Archive"
slug:   "All the Archive Intros"
---

King's College at Cambridge has a nice [introduction to work in the archives](http://www.kings.cam.ac.uk/archive-centre/introduction-archives/overview.html) that we'll leverage. If you could please read sections 1-6, that would be lovely.

(I often phrase things as requests. Students have sometimes expressed confusion. You need to read these six sections.)

---
week:   "1"
day:    "4"
title:  "Day 4: Finding Stories"
type:   "Reading"
slug:   "A bit of research..."
---

In class, you will have been assigned a "type" of immigrant. Your job is to do some research... you're looking for "stories of immigration" around the theme that you found.

The four categories are "student," "child," "parent," "single (young adult) person." I describe these more below.

Identify 4-6 (or more) stories online that provide some rich context for the lives of these people. These might be videos, blog posts, articles in newspapers, magazines, podcasts, and so on. We will collect the URLs for these resources in class, and talk about our next steps with them. (They will form a backbone for some of our work over the weekend.)

I might be abusing the word "immigrant" below. By it, I mean "someone who has, under some means, come to the USA to study, work, or live." 

Note, you're just looking for resources for Friday. Skim. Decide if they're resources worth further investigation.

## Student

You're 18, and have decided to come to the US to study at Bates College. See if you can find some stories online that reflect the various aspects of what was involved in making that jump. For example, if you're a Bates student who 1) grew up in Portland, and 2) decided you wanted to stay in Maine, then 3) it is no mystery how you learned about Bates. You probably were able to come up and visit without difficulty any number of times. However, if you were (say) coming to Bates from China, India, Brazil, or anywhere else in the world, you may not have had an opportunity to visit. In fact... how did you even *learn* about Bates?

Find some stories that reflect students' experiences in making the move to the US to study. Ideally, those stories should reflect the process of making their way to a small liberal arts college (so that their experience is more relatable to you). Consider all aspects of what it is like making the transition to college: the selection, application, the interview, and so on as you attempt to find stories online.

## Child

You're now 18, but when you were 2, your parents came to the US without documentation. Your parents have done everything they can to help you succeed and value education, and as a result, you have earned a place at a nationally ranked, small liberal arts college. However, you live in an ambiguous space: because your parents were undocumented in their move to the US, you, too, are considered undocumented.

What are the implications for students in this space? What experiences do/did students growing up, for all intents and purposes, "American," but living with the reality that they are (in the eyes of the government/law) most definitely *not* "American."

I am not interested in political rants that take stands on the question of your peers who live in this space. Instead, I am interested in you finding stories about what this state of affairs means, as a lived experience, for college-age people who are living and studying in the US as "undocumented" immigrants.

## Single Person

You have left your home country---and, for that matter, your home, and everyone you know---for the United States. You did so to take advantage of opportunity in the tech sector in San Francisco, because this is where all the most exciting opportunities are. Having recently completed a degree in Computer Engineering, you enter the US on an H-1B visa, and now work at Giggle, a major providor of search services for funny videos. (You're confident it will be the next big thing.)

What was the process like for you to get an H-1B visa? What is life like in San Francisco? What kind of salary are you earning, given your degree and field, in San Fran? Consider that your visa status might impact your situation. In short: can you find stories of what life is like for "tech immigrants"?

## Parent

You came to the US, seeking asylum for you and your two children.

What was that process like? What did you have to go through? 


# Submission

[Submit your links here](https://goo.gl/forms/r0qfe9f4PyDY6nVo1).

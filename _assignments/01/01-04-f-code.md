---
week:   "1"
day:    "4"
title:  "Day 4: Introducing Python"
type:   "Code"
slug:   "A first jump into Python notebooks."
---

For Friday, attempt the first notebook. If it goes well, attempt the second.

1. Right click and download the Chapter 1 notebook <a href="{{site.baseurl}}/resources/notebooks/Chapter_1.ipynb" download>Chapter_1.ipynb</a>. This will leave the file in your Downloads folder.
2. Go to Catapult, open your notebooks.
3. Click "Upload" (on the right), find your Chapter_1.ipynb file in your Downloads folder, select it and say "OK" (or whatever word your computer uses to say "do this thing").

Now, you'll have a copy of the notebook in Catapult.

If you feel like the first notebook goes well, attempt the second <a href="{{site.baseurl}}/resources/notebooks/Chapter_2.ipynb" download>Chapter_2.ipynb</a>. However, if you find you have questions, just take a deep breath, walk away from the keyboard, and come on in. I meant for you to be doing this with a friend initially. If tossing you in for your first taste is a bit much, DON'T PANIC.

---
week:   "1"
day:    "0"
title:  "Day 2: The Civil Rights Context"
type:   "Reading"
slug:   "Children encaged... it was like we were animals."
---

Over the course of the term, our work will explore immigration and, therefore, fundamental questions what it "means to be an American." This, therefore, is *intimately* tied up with questions of civil rights and, in the US, our constructions of race.

**I, personally, find it difficult to imagine historical contexts**. Or, perhaps, I know so little of the past that it is difficult for me to imagine what my *day to day life* would be like if I lived in, say, Lewiston in 1918. So, I will be doing this work with you as the term progresses, and will share my work publicly as I go, as it the historical exploration represents a learning journey for me as well.

To set the stage for our work this term, let's begin with the civil rights movement of the 1950s and 1960s.

## Phase 1: Reading

First, I have three videos. These were challenging for me to watch, and might be for you as well.

The first video provides some broad-strokes context for the Birmingham protests, and features interviews with people who were present and part of those protests. **Note their words.**

The second video provides additional context for the writing of King's *Letter from a Birmingham Jail*, which we will read and discuss soon.

The final video has no narration, but is a collection of footage from the Birmingham protests. Hard to watch, if you turn off all your distractive devices, and imagine yourself in that place.

1. [PBS: The Birmingham Campaign](http://www.pbs.org/black-culture/explore/civil-rights-movement-birmingham-campaign/)
2. [Freedom Comes to Birmingham](https://www.youtube.com/watch?v=Y_9rSku_1o4)
3. [Images from Birmingham](https://www.youtube.com/watch?v=KFcCQDkVOjM&has_verified=1)

Then, AFTER watching the videos, read [PUBLIC STATEMENT BY EIGHT ALABAMA CLERGYMEN](http://www.massresistance.org/docs/gen/09a/mlk_day/statement.html). This letter was written on April 12th, during the time of King's incarceration, during the protests you have just learned more about.

## Phase 1: Respond

In Google Drive, in your DCS103 folder, create a new GDoc. Title it "Alabama Clergymen Reflection." 

First, imagine that you are member of the Alabama clergy who wrote this letter.

* What motivated you to write this?
* What are you feeling about the culture and climate of your hometown?
* What are you feeling about the culture and climate of the country?

Second, imagine you are MLK Jr., sitting in a Birminham jail.

* What is your first response to this letter? How does it make you feel?
* What are all of the things that are going through your mind? 

## Phase 2: Listening

**Do this listening after you have written your responses to Phase 1, not before.** 

[On Point: So You Want To Talk About Race](http://www.wbur.org/onpoint/2018/01/15/mlk-ijeoma-oluo)

## Phase 2: Response

I'm going to ask your first response to this piece to be threefold:

1. What is the most interesting (which might be challenging, inspiring, frustrating, confusing) thing you heard during the interview portion of the piece?
2. What caller comment did you find you most agreed with or related to? 
3. What caller comment made you most uneasy, and why?

If you're uncertain how to respond to any one of these questions, explore that. In either case, I'm looking for you engage with at least 2-3 paragraphs. It might be 1 paragraph for each. It might be 4 or 5. Take the space you need to explore the ideas.

## Rubric

As we are using a contract-based model for assessment, and this is our first assignment, it will be assessed in one of four categories: Major Revision, Minor Revision, Meets Contract, or Exceeds Contract.

## Submission

These will essentially be the instructions for every assignment this term. 

In short:

* Do not put your name in the document.
* Submit a PDF whenever possible.
* Upload your work to Lyceum.


**Please DO NOT put your name on your document**. I am going to experiment with anonymous assessment/feedback this term; I believe there is good evidence that this will improve the *equity* of my feedback. Lyceum knows who you are, and I do not need (or, even, want!) your name embedded in the document itself. 

**Please save and submit your work as a PDF**. I have tools that make it easy to provide feedback on PDFs, and they work best across all computing platforms. 

* From Google Docs, you can click "File," then "Download as...", and select "PDF".
* [Mac Instructions](https://support.apple.com/kb/PH25326?locale=en_US)
* [Windows instructions](https://support.office.com/en-us/article/save-or-convert-to-pdf-or-xps-d85416c5-7d77-4fd6-a216-6f4bf7c7c110).
* On a Chromebook, you can save as PDF, but that's because a Chromebook is using Google Docs.

I have set up our Lyceum site for the course, and [assignment submission and feedback](https://lyceum.bates.edu/mod/assign/view.php?id=210240) will be handled through Lyceum. 

So, you can [submit your work](https://lyceum.bates.edu/mod/assign/view.php?id=210240) there. I will try and make sure to always link to the submission spot. However, if I don't, just jump into Lyceum and find the assignment. 




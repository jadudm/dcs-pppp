---
week:   "12"
day:    "0"
title:  "Day 35: Towards a Conclusion"
type:   "In Class"
slug:   "Beginning our Data Stories"
---

In class, we will do three things:

1. **Demo example notebook**. I will begin with a demonstration notebook that you can use as a starting point for explorations of the map data. This is not a new notebook, it is just a revision of the notebooks we've seen before with correct and clean data.

1. **Discuss our final essay**. We will discuss what we think might be critical components of, and criteria for assessing, our final essay. We will finalize these criteria on Wednesday.

1. **Grab a partner, and plan**. 

To use the demo notebook:

1. Create a folder in your Notebooks area for this work. "DataStory" might be a folder name. "FinalDataEssay" might be another good folder name. Either way, choose something that makes sense to you. 
1. Download the <a href="{{site.baseurl}}/resources/notebooks/PPPP112.ipynb" download>most recent PPPP library</a>.
1. Download the <a href="{{site.baseurl}}/resources/notebooks/MappingStartingPoint.ipynb" download>example notebook</a>.
1. Add your MapBox API key to the example notebook. You'll have to scroll down to find where it goes, and you'll have to uncomment the code where the key addition happens. 

If you have questions or problems with any of this, *please ask*. You don't want to get stuck on these things; it isn't the interesting bit. (In fact, remember: you should not spend more than 15-20 minutes wrestling with something, ever. **You should be asking questions**, of a partner or of me. Better to ask questions, learn, and make progress than to sit stumped and frustrated!)
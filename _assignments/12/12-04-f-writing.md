---
week:   "14"
day:    "4"
title:  "Final Presentations"
type:   "Writing"
slug:   ""
---

We have, as a group, decided that we would extend our work on our final projects into the final exam period. This does give you more time to bring depth and polish to your work, which (from working with you) is diverse and interesting work. I look forward to your presentations.

## Collaborative Submission: The Project

You should bring together your project work in an organized manner, and submit it to Lyceum under the "Final Project" slot. You should include *all* of the documents that are part of the project; for example, if you produced a report, and mapping/analysis was part of that report, please include the notebook that you used to do the computation behind the report. 

* If you developed a Jupyter notebook as part of your work, then it should be downloaded (as an .ipynb file), and submitted. 

* If a report was written (in addition to a notebook), then please submit it as *both* the original format (eg. a Word document) as well as a PDF version of the report. If you wrote your report in Google Docs, then please download a version in both Word format as well as PDF. 

* If a slide deck was assembled for the presentation (I suspect one will be), please include both the source and a PDF of that presentation. (For example, if you use Powerpoint, include the .pptx file as well as a PDF of the presentation. If you use Google Slides, download both a .pptx as well as a PDF of the presentation.) I am aware animations/etc. may not come through in a PDF. (Including the slide deck is largely for reference and completeness.)

If this is work that was submitted collaboratively, then only one person should submit the work. However, you *must* make sure that the collaborators are all acknowledged on *all* of the work. (This is *not* an anonymous submission.)

## Individual Submission: Statement of Collaboration

On an individual basis, you should each submit a short document that makes clear the nature of your collaboration with others on this project (from your own, personal perspective). You should spend no more than one paragraph per teammate/collaborator (including one for yourself). This should be a short, factual description of the nature of the work and effort contributed to the project. 

This will be submitted to Lyceum as a PDF under the **Statement of Collaboration**.

## Individual Submission: Contractual Self Evaluation

Our work this term was executed against the backdrop of a [contract](https://docs.google.com/document/d/1-jGY2_V63IImwnRtvTtl_8KGwjR2ndlrdmRrNpDswUo/). Specifically, we felt that work that *meets contract* would:

* Complete all work for the course, and meet all deadlines.
* Participate fully in class, without distraction.
* Honestly attempt to demonstrate an awareness of and respect for self and others through their writing and interactions with colleagues.
* Be on time and present for work arranged in community with others.
* Engage in revision and the process of learning, where necessary, to meet contract.
* Ask questions in class and make appointments to clarify understanding.

Meeting contract means that you did *above average work*. It does not mean you did *excellent* work. 

Your self-evaluation should reflect critically (but, briefly) on each of these points. The result of this self-evaluation should allow you to make an estimation of whether your efforts and output this term reflect an overall outcome of failing to meet the contract, meeting the contract, or exceeding the contract. If you feel you (overall) fell short or achieved beyond the contract, you should indicate in which ways, and what you believe that means in terms of the evaluation of your work.

This will be submitted to Lyceum as a PDF under the **Contractual Self-Evaluation**.
---
week:   "7"
day:    "2"
title:  "Day 21: The EDA Exploration"
type:   "Code"
slug:   "For clarity..."
---

Start the {% include nblink file="IntroEDA"%} notebook; work through at least 3 of the datasets, preferably all six. (The last two or three are fun. Perhaps work through them backwards... the dataset does not change the nature of the work.)

We will start the dinosaur datasets on Wednesday. This is practice. You should come in with questions on Wednesday.
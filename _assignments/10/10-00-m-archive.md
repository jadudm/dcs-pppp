---
week:   "10"
day:    "0"
title:  "Day 29: Filtering Data"
type:   "Archive"
slug:   "All the filters..."
---

Because I will be traveling, I'm making the deadline on the data filtering notebook on Monday. Remember, the notebook and data are in Lyceum.

## Submission

Submit your notebook to Lyceum.

---
week:   "10"
day:    "4"
title:  "Day 31: Analysis Begins"
type:   "In Class"
slug:   "The Beginning of the Story"
---

At this point, we will begin our data stories. During class, we will work together to discuss possible stories, approaches to analysis, and start bringing our work together as a unified whole.

The data is filtered, we know how to plot basic charts, and we'll circle around and make sure we're clued in to how to map our data. (We'll have to set some things up again.) This will give you time over break to do some thinking and planning.


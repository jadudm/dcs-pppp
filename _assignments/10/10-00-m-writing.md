---
week:   "10"
day:    "0"
title:  "Day 29: Take Home Practice"
type:   "Writing"
slug:   "Practice for the Final"
---

The practice take-home will be due today. When it is done being written, it will be linked in here.

# Submission

Upload to Lyceum.
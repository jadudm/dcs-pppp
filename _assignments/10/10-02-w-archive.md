---
week:   "10"
day:    "2"
title:  "Day 30: Revisiting Mapping"
type:   "Archive"
slug:   ""
---

Re-read the [Introduction to Mapping](https://lynxruf.us/courses/dcs103f18/a/05/05-00-m-inclass.html) so we can dive back in a bit in class.

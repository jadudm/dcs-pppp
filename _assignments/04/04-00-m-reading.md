---
week:   "4"
day:    "0"
title:  "Day 11: Studying Programming Ch 7"
type:   "Reading"
slug:   ""
---

Chapter 7 of *Studying Programming*. No particular response requested on the reading. It will be assumed to be background knowledge that you now have.
---
week:   "4"
day:    "2"
title:  "Day 12: Practice"
type:   "Code"
slug:   "All the functions"
---

In class, we began working with 'if' statements. This involved the {% include nblink file="if-practice" %} notebook. 

Complete the notebook for Wednesday, and come to class with questions.



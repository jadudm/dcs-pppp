---
week:   "4"
day:    "0"
title:  "Day 11: Introducing if, Newspapers"
type:   "In Class"
slug:   ""
---

For Wednesday, we'll be working with some old newspapers. Your assignments were generated with {% include nblink file="PaperMixing" %}.

In class, we'll begin working with 'if' statements. You will want to grab the {% include nblink file="if-practice" %} notebook.




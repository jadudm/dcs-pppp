---
week:   "4"
day:    "0"
title:  "Scanning Should Be Complete"
type:   "Archive"
slug:   "Scanning should be done by now."
---

You should have completed scanning your logbook by class on Monday.

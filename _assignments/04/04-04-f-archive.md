---
week:   "4"
day:    "4"
title:  "Day 13: Digging into the papers: 1913-1918"
type:   "Archive"
slug:   "If only it smelled like newsprint..."
---

**HINT**: Do 8-12 on Tuesday, and 8-12 on Thursday. This will get you to Friday without being overwhelmed.

This assignment is a beginning. We are going to build a *database* of text from the past 100 years that we can then analyze, computationally, exploring the kind of language used over time regarding immigrants. 

For the first round, you have been assigned a specific state. Using the [Chronicling America](https://chroniclingamerica.loc.gov/) website (provided by the Library of Congress), you'll dive into your state's newspapers from the years 1913-1918. Try and find 3-4 articles from each year---perhaps one every two months or so---that explore immigration in some way. This will give us around 16-24 articles from your state from that timeperiod. If we all do this, we'll have around 550 articles from across the nation that we can use to study how people were talking about and thinking about immigration at the time.

For each newspaper article, [complete the database form](https://goo.gl/forms/pXCpssBH63JR2wGg1). This will give us a spreadsheet that contains all of the text of all of the articles that you've found. For the form:

1. Select your newspaper from the drop-down.
2. In the website interface, download the PDF of the article that you've found. Attach it to the form.
3. Also in the interface, select the "text" option. Select the text from this view that you believe best represents your article, and paste it into the form. *This text will be messy. Your best effort here is appreciated by all of us, as you will discover.*

You'll complete that form once for every article that you find. Don't worry: it won't overwrite your previous articles. 

Your state assignments are below.

----


**AB**: Virginia

**AM**: Illinois

**ANS**: New Jersey

**BG**: Texas

**BL**: Kentucky

**BM**: California

**BvP**: Montanna

**CP**: Maryland

**CS**: Connecticut

**CU**: Hawaii

**EB**: Vermont

**HP**: Nebraska

**JM**: Alaska

**KN**: New Mexico

**MB**: South Carolina

**MD**: New York

**ML**: Indiana

**MM**: West Virginia

**NAT**: Minnesota

**NK**: Washington

**RH**: Idaho

**RJ**: North Carolina

**TR**: District of Columbia
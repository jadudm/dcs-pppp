---
week:   "4"
day:    "0"
title:  "Day 11: Think Python"
type:   "Code"
slug:   "Chapter 5.1 - 5.7"
---

Read [Chapter 5.1 through 5.7 of Think Python](http://greenteapress.com/thinkpython2/html/thinkpython2006.html). 

As you read, create a notebook where you actively engage with all aspects of the chapter that involve code. Instead of me creating the notebook for you, you should take control over your own learning, and explore each of the active/programming parts of the chapter. This isn't a lot of work, but it is a critical part of understanding the material.

## Submission

Upload your notebook to Lyceum.
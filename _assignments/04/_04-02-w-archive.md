---
week:   "4"
day:    "2"
title:  "Day 12: Digging into the papers: 1913-1918"
type:   "Archive"
slug:   "If only it smelled like newsprint..."
---

This assignment is a beginning. We are going to build a *database* of text from the past 100 years that we can then analyze, computationally, exploring the kind of language used over time regarding immigrants. 

For the first round, you have been assigned a state. Using the [Chronicling America](https://chroniclingamerica.loc.gov/) website (provided by the Library of Congress), you'll dive into newspapers from your state from the years 1913-1918. Try and find 4-6 articles from each year---perhaps one every two months or so---that explore immigration in some way. This will give us around 16-24 articles from your state from that timeperiod. If we all do this, we'll have around 550 articles from across the nation that we can use to study how people were talking about and thinking about immigration at the time.

*If this goes quickly, please consider doing a few more articles. It does not hurt us to have more data.*


**AB**: Virginia

**AM**: Illinois

**ANS**: New Jersey

**BG**: Texas

**BL**: Kentucky

**BM**: California

**BvP**: Montanna

**CP**: Maryland

**CS**: Connecticut

**CU**: Hawaii

**EB**: Vermont

**HP**: Nebraska

**JM**: Alaska

**KN**: New Mexico

**MB**: South Carolina

**MD**: New York

**ML**: Indiana

**MM**: West Virginia

**NAT**: Minnesota

**NK**: Washington

**RH**: Idaho

**RJ**: North Carolina

**TR**: District of Columbia

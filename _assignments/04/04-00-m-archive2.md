---
week:   "4"
day:    "0"
title:  "Scanning Teams"
type:   "Archive"
slug:   "A partner with whom to scan all the things"
---

Your mission is to each scan a census logbook. We estimate scanning a logbook will take roughly 40-50 minutes, total. By each scanning one book, we will be taking advantage of the power of many hands to accomplish a task that would otherwise take one person 24 hours.

<iframe width="560" height="315" src="https://www.youtube.com/embed/cI2SOcGRnco?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

*You must borrow a USB stick from the librarians along with your book in order to complete this project.* This will generate a lot of large files, and the USB stick is necessary.

Perhaps most important to this process is that you capture the *metadata* about the scanning. [You will use a Google Form to upload your images]().

**Group 1**
* AM
* MD

**Group 2**
* HP
* MB

**Group 3**
* DH
* AB

**Group 4**
* CS
* NK

**Group 5**
* MM
* BL

**Group 6**
* KN
* BM

**Group 7**
* BG
* EB

**Group 8**
* RH
* ML

**Group 9**
* NAT
* TR

**Group 10**
* CU
* ANS

**Group 11**
* JZ
* BvP

**Group 12**
* CP
* RJ

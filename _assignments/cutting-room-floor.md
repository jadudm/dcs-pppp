
<div class = "text-center mx-auto d-block">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/llnSPaJdews" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
</div>

What is the choreographer saying with this piece? Is the technology amplifying the message, or an appendage?

<div class = "row">
    <div class = "col-sm-6 text-center mx-auto ">
        <iframe width="560" height="315"     src="https://www.youtube.com/embed/C696tYQvKkg" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
    </div>
</div>
<div class = "row ">
    <div class="col-sm-6 offset-sm-3">
        Although a decade old, what technologies are being leveraged in Regnier's work?     
    </div>
</div>



<div class = "row">
    <div class = "col-sm-6 text-center mx-auto d-block">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/DNe0ZUD19EE" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
    </div>
</div>
<div class = "row ">
    <div class="col-sm-6 offset-sm-3">
        What is a marching band but choreography on a massive scale? Modern half-time shows are made possible by technology—programs that transform representations of positions and animations on screen into marching patterns for every member of the band. Does this enhance, or destroy, the art of choreographing a half-time show?
    </div>
</div>
<div class = "text-center mx-auto d-block">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/VNSmFxDfVn0" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
</div>

It was choreographed, but does the synchrony inspire? Is the technology being used to challenge us, or make us reflect on our humanity through the dancers' own inhumanity? Or, is it just a bunch of toy robots programmed to move in synch?


OK Go White Knuckles
<iframe width="560" height="315" src="https://www.youtube.com/embed/qZxxzNNXuiw" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
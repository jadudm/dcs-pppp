---
week:   "0"
day:    "4"
title:  "Day 1: Help Gidget"
type:   "In Class"
slug:   "Gidget is your friend."
---

[Gidget](https://www.helpgidget.org/) is a robot. And it's your job to help gidget save puppies. And chickens. And stuff.

Although light hearted, the Help Gidget environment exemplifies all of the kinds of programming activities we'll be engaging in this semester. Today, you'll dive in and, with a partner, dive in.

# Rules

1. **Take turns driving**. After each puzzle, switch drivers. You are going to remember and learn better when you're *actively doing* as opposed. Also, talk to your navigator: let them know what you're thinking when you're working.
2. **Navigate actively**. When you're not driving, you should be focused. Pay attention to what your partner is doing, and offer help as needed.
3. **Focus**. Close Facebook, Snap, Wiggle, or whatever the hell else you have to distract you. Put the damn phone away. Focus, for one hour, on *just one thing*. Nothing will blow up, die, or go to hell in a handbasket if you put your phone away for the duration of class.

# While You're Working

Make note of two things:

1. Things you think are interesting or surprising.
2. Things you think are difficult, or that slowed you down/you got stuck on.

It's a good idea to write down questions that you have, when you have them.

# When You're Done

[Submit your thoughts, stuck-things, and questions](https://goo.gl/forms/HEG9V4Jqi42fnrgG3).

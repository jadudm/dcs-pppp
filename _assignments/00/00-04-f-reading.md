---
week:   "0"
day:    "4"
title:  "Day 1: Studying Programming"
type:   "Reading"
slug:   "Chapters 2 and 3"
---

Please read chapters two and three of *Studying Programming*, which is available in [readings folder]({{site.urls.readings}}). To augment your reading of chapter 2, you might watch [this short animation on computer architecture](https://www.youtube.com/watch?v=AkFi90lZmXA). The content is accurate, despite it's fun presentation.

# While Reading

As you're reading, make notes of terms and definitions. 

# When Done

When you're done, develop ten questions about chapter two:

* Three true/false
* Four multiple choice
* Three fill-in-the-blank.

Create a folder in your Google Drive for this course, and create a Google Doc in that folder for these questions. Write these up in that document; we'll come back to these. 

Copy the questions from your GDoc [into the submission form](https://goo.gl/forms/SzcdtVd0B5Jf5Vbm2).



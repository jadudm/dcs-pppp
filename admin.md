---
title: Admin
layout: default
---

# Administrative Details

Things about the operation of the course that you may want to check out from time to time.

* [Office Hours Booking Thing]({{site.calendarbooking}})
* [Course Syllabus]({{site.syllabus}})
* [Course Grading Contract]({{site.grading_contract}})
* [Course Lyceum Site]({{site.lyceum}})
